(function($) {
    tinymce.create("tinymce.plugins.Directionality", {
        init : function(ed, url) {
            var t = this;
			t.editor = ed;
	
			function change_dir(dir) {
				var dom = ed.dom, curDir, blocks = ed.selection.getSelectedBlocks();

				if (blocks.length) {
					curDir = dom.getAttrib(blocks[0], "dir");

					tinymce.each(blocks, function(block) {
						// Add dir to block if the parent block doesn't already have that dir
						if (!dom.getParent(block.parentNode, "*[dir='" + dir + "']", dom.getRoot())) {
							if (curDir != dir) {
								dom.setAttrib(block, "dir", dir);
							}
						}
					});

					ed.nodeChanged();
				}
			}
			
			
			//add to toolbar
			var toolbar = '<ul class="aui-buttons">'+
							'<li class="toolbar-item aui-button" id="rte-ltr">' + 
								'<a class="toolbar-trigger" href="#" id="rte-source-editor" title="LTR">' + 
									'<span class="trigger-text">LTR</span>'+
								'</a>'+
							'</li>'+
							'<li class="toolbar-item aui-button" id="rte-rtl">' + 
								'<a class="toolbar-trigger" href="#" id="rte-source-editor" title="RTL">' + 
									'<span class="trigger-text">RTL</span>'+
								'</a>'+
							'</li>'+
						'</ul>'
							
			var helpGroup = $('#rte-button-justifyleft').parent('ul');
            helpGroup.before(toolbar);
			
			var ltr_button = $('#rte-ltr');
			ltr_button.click(function (e) {
				e.preventDefault();
				change_dir('ltr');
			});
			
			var rtl_button = $('#rte-rtl');
			rtl_button.click(function (e) {
				e.preventDefault();
				change_dir('rtl');
			});
		
			function addPixels(el, cssStyle, add) {
				var px = /(\d*)(px)?/;
				var match = px.exec(el.css(cssStyle));
				var curWidth;
				if(match.length > 1) {
					curWidth = +match[1];
					curWidth += add;
					el.css(cssStyle, curWidth + 'px');
				}
			}
			addPixels($('#rte-toolbar .aui-toolbar2-primary'), 'margin-right', 90);
			ed.onNodeChange.add(t._nodeChange, t);
			
			//fix rejump on rtl shift tab
			function rtlFixNeeded(ed, e) {
				var dom = ed.dom, blocks = ed.selection.getSelectedBlocks(), node = ed.selection.getNode();
				
				var rtl_mode = dom.getAttrib(blocks[0], "dir")=='rtl';
				return e.keyCode === tinymce.VK.TAB && e.shiftKey && rtl_mode &&
					(ed.queryCommandState('InsertUnorderedList') || ed.queryCommandState('InsertOrderedList'));
			}
			
			function fixShiftTab(ed, e) {
				if(rtlFixNeeded(ed, e))
				{
					ed.execCommand('Outdent', true, null);
					ed.execCommand('Outdent', true, null);
					
					var node = ed.selection.getNode();
					node.style.setProperty('text-align','right');
					
					return false;
				}
			}
			ed.onKeyDown.add(fixShiftTab);
        },
        getInfo: function() {
            return {
                longname: "Directionality",
                author: "Moxiecode Systems AB",
                authorurl: "http://tinymce.moxiecode.com",
                infourl: "http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/directionality",
                version: tinymce.majorVersion + "." + tinymce.minorVersion
            }
        },
        _nodeChange : function(ed, cm, n) {
			var dom = ed.dom, dir;

			n = dom.getParent(n, dom.isBlock);
			if (!n) {
				cm.setDisabled('ltr', 1);
				cm.setDisabled('rtl', 1);
				return;
			}

			dir = dom.getAttrib(n, 'dir');
			cm.setActive('ltr', dir == "ltr");
			cm.setDisabled('ltr', 0);
			cm.setActive('rtl', dir == "rtl");
			cm.setDisabled('rtl', 0);
		}
    });
    tinymce.PluginManager.add("directionality", tinymce.plugins.Directionality)
})(AJS.$);

AJS.Rte.BootstrapManager.addTinyMcePluginInit(function(settings) {
	settings.plugins += ",directionality";
	settings.theme_advanced_buttons2 += ",rtl,ltr";	
});